# Money Gang App
## Mini Challenge 1

| Branch Name      | Description |
| ---------------- | ----------- |
| master           | Hold stable releases from develop      |
| develop          | Hold development state app then will be merged to master branch |
| feature-branch   | Hold development state of a feature then will be merged to develop branch | 

> feature-branch will be named accordingly based on the feature name. (e.g. base-app-setup, add-data-observer, etc)
