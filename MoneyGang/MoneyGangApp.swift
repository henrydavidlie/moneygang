//
//  MoneyGangApp.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

@main
struct MoneyGangApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
