//
//  ChartItems.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import Foundation

enum ChartItems: String, CaseIterable {
    case week = "1W"
    case month = "1M"
    case year = "1Y"
}
