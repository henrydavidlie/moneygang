//
//  ContentView.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct ContentView: View {
    
    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = .blue
        UISegmentedControl.appearance().setTitleTextAttributes(
            [.foregroundColor: UIColor.white], for: .selected)
    }
    
    var body: some View {
        TabView {
            ListScreen()
                .tabItem {
                    Label("Lists", systemImage: "list.dash")
                }
            
            AnalyticsScreen()
                .tabItem {
                    Label("Analytics", systemImage: "chart.bar.fill")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
