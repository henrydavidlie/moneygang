//
//  ListScreen.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct ListScreen: View {
    @State private var listState: ListItems = .current
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    HStack {
                        Spacer()
                        Button(action: {}) {
                            Image(systemName: "plus.circle.fill")
                                .resizable()
                                .frame(maxWidth: 40, maxHeight: 40)
                        }
                        .padding()
                    }
                    .offset(y: -60)
                    Spacer()
                }
                VStack {
                    Picker("Tabs:", selection: $listState) {
                        ForEach(ListItems.allCases, id: \.self) {
                            Text($0.rawValue)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .padding()
                    switch listState {
                    case .current:
                        CurrentScreen()
                    case .history:
                        HistoryScreen()
                    }
                    Spacer()
                }
            }
            .navigationTitle("Shopping Lists")
        }
    }
}

struct ListScreen_Previews: PreviewProvider {
    static var previews: some View {
        ListScreen()
    }
}
