//
//  CurrentScreen.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct CurrentScreen: View {
    @State private var showReceiptModal = false
    
    var body: some View {
        VStack {
            Button("Done") {
                showReceiptModal.toggle()
            }.sheet(isPresented: $showReceiptModal) {
                ReceiptModal()
            }
        }
    }
}

struct CurrentScreen_Previews: PreviewProvider {
    static var previews: some View {
        CurrentScreen()
    }
}
