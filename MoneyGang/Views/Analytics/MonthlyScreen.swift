//
//  MonthlyScreen.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct MonthlyScreen: View {
    var body: some View {
        Text("Monthly")
    }
}

struct MonthlyScreen_Previews: PreviewProvider {
    static var previews: some View {
        MonthlyScreen()
    }
}
