//
//  ListItems.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import Foundation

enum ListItems: String, CaseIterable {
    case current = "Current"
    case history = "History"
}
