//
//  AnalyticsScreen.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct AnalyticsScreen: View {
    @State private var chartState: ChartItems = .week
    
    var body: some View {
        NavigationView {
            VStack {
                Picker("Tabs:", selection: $chartState) {
                    ForEach(ChartItems.allCases, id: \.self) {
                        Text($0.rawValue)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                switch chartState {
                case .week:
                    WeeklyScreen()
                case .month:
                    MonthlyScreen()
                case .year:
                    YearlyScreen()
                }
                Spacer()
            }
            .navigationTitle("Analytics")
        }
    }
}

struct AnalyticsScreen_Previews: PreviewProvider {
    static var previews: some View {
        AnalyticsScreen()
    }
}
