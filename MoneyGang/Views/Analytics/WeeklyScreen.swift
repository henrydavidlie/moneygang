//
//  WeeklyScreen.swift
//  MoneyGang
//
//  Created by Henry David Lie on 06/04/21.
//

import SwiftUI

struct WeeklyScreen: View {
    var body: some View {
        Text("Weekly")
    }
}

struct WeeklyScreen_Previews: PreviewProvider {
    static var previews: some View {
        WeeklyScreen()
    }
}
