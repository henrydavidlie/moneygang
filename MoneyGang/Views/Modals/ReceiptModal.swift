//
//  ReceiptModal.swift
//  MoneyGang
//
//  Created by Henry David Lie on 07/04/21.
//

import SwiftUI

struct ReceiptModal: View {
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        NavigationView {
            VStack {
                Button("Press to dismiss") {
                    presentationMode.wrappedValue.dismiss()
                }
            }
            .navigationTitle("Upload Receipt")
            .navigationBarItems(
                leading:
                    Button("Cancel") {
                        presentationMode.wrappedValue.dismiss()
                    },
                trailing:
                    Button("Done") {
                        
                    }
            )
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ReceiptModal_Previews: PreviewProvider {
    static var previews: some View {
        ReceiptModal()
    }
}
